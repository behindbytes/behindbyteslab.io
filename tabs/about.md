---
title: About

# The About page
# v2.0
# https://github.com/cotes2020/jekyll-theme-chirpy
# © 2017-2019 Cotes Chung
# MIT License
---

Behind Bytes is just another tech blog.

This blog is a work in progress and there might be certain broken links.

If you find some errors in the content, please report to [author@behindbytes.com](mailto:author@behindbytes.com).

Welcome and happy reading.
