---
layout: post
title:  "How to use rebase to drop an old commit"
subtitle: "git rebase"
date:   2020-05-13 10:24:07
categories: [development, revision-control]
tags: git
description: "git rebase drop a commit"
published: true
canonical_url: https://behindbytes.com/development/2020/05/13/how-to-use-rebase-to-drop-an-old-commit.html"
---

Some times you would want to completely remove a commit in the middle of the git tree. This post tells you how to do that with
git rebase.

Let us try to drop "commit 8" in our [git-demo](https://github.com/behindbyte/git-demo)

Note: Git rebase would rewrite the git tree. Please see [here](https://behindbytes.com/development/2020/05/12/how-git-rebase-works.html)

## Drop a commit using rebase

Suppose you want to drop the 10th commit, copy the commit ID of the commit below that(11th commit).

In the below example, we wanted to drop the commit with subject "commit 8". So we select the commit ID of
"commit 7"(one commit below) and start rebase as follows

```bash
git rebase -i d7589a009629f7a57f8a98280f2cc1521d19b72f
```

This dropping also leads to a conflict. We need to fix that too. Please see [this post](https://behindbytes.com/development/2020/05/12/how-git-rebase-fix-conflicts.html) on how to resolve conflicts.

![](/assets/gifs/git/0002-git-rebase-drop.gif)
