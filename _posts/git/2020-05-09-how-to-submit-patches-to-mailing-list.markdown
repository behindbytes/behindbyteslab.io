---
layout: post
title:  "How to submit patches to mailing list"
subtitle: "Series: git-send-email[1/2]"
date:   2020-05-09 10:24:07
categories: [development, revision-control]
tags: git
description: "Sending patches to mailing list via git-sendemail"
published: true
canonical_url: https://behindbytes.com/development/2020/05/09/how-to-submit-patches-to-mailing-list.html"
---

Several opensource projects, like linux kernel accepts patches via mailing list. You would have to mail the patches to them as plain text(not as an attachment).

For projects using git, you could use git's send-email feature to prepare and send patches to the mailing list.
We are going to show how to configure your gmail account to send patches using git send-email.

## Installing git-sendemail
Below are the installation instructions.

```bash
sudo apt-get update
sudo apt-get install git-email
```

## Configuring git-sendemail

Once the package is installed, we need to setup the necessary parameters for git-sendemail to use your Gmail account to send the patches.

Edit your ~/.gitconfig file and add the below lines.
```
[sendemail]
        smtpserver = smtp.gmail.com
        smtpserverport = 587
        smtpencryption = tls
        smtpuser = <your email address>
        smtppass="your email password"
```
You could also skip the smtppass field if storing password as plaintext concerns you. In that case, git send-email would display a prompt for you to enter the password while sending the patches.

For example:
```
[sendemail]
        smtpserver = smtp.gmail.com
        smtpserverport = 587
        smtpencryption = tls
        smtpuser = developer@behindbytes.com
        smtppass="12345678"
```

## Sending patches

### Sending a single patch
Create a simple patch file using git format-patch -1 in your repo. Lets test the configuration by sending the patch to self.

```bash
git send-email --to=<email> <path to patch file>
```
For ex:
```bash
git send-email --to=developer@behindbytes.com 0001-example.patch
```
### Sending a patch series
Sometimes you would want to send multiple patches as a series. For that take a backup of the patches in a directory.

Example to send 2 patches from top of the tree. From inside the git repository run.

```bash
mkdir patches
git format-patch -2 -o ./patches
git send-email --to=developer@behindbytes.com ./patches/*
```

You can also directly send them without using git format-patch to prepare the patch first.
```bash
git send-email -2 --to=developer@behindbytes.com
```

