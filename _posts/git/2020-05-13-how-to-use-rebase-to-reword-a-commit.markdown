---
layout: post
title:  "How to use rebase to reword a commit"
subtitle: "git rebase"
date:   2020-05-13 10:24:07
categories: [development, revision-control]
tags: git
description: "git rebase reword a commit"
published: true
canonical_url: https://behindbytes.com/development/2020/05/13/how-to-use-rebase-to-reword-a-commit.html"
---

If you want to reword the top commit's message, you would just do "git commit --amend". If you wanted to reword the
commit message of say 10th commit below HEAD, then what would you do? You could use git rebase.

Note: Git rebase would rewrite the git tree. Please see [here]( https://behindbytes.com/development/2020/05/12/how-git-rebase-works.html)

## Reword using rebase

Suppose you want to reword the 10th commit, copy the commit ID of the commit below that(11th commit).

In the below example, we wanted to add some more words to commit with subject "commit 8". So we select the commit ID of
"commit 7"(one commit below) and start rebase as follows

```bash
git reabse -i d7589a009629f7a57f8a98280f2cc1521d19b72f
```

![](/assets/gifs/git/0001-git-rebase-reword.gif)
