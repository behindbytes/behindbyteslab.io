---
layout: post
title:  "OpenSBI[Part-2]: Building and running OpenSBI in QEMU"
date:   2020-07-07 00:00:00
categories: [riscv, opensbi]
tags: [opensbi, riscv]
published: true
---

Let us see how to build and run OpenSBI in QEMU.

### Setting up the Toolchain

Download the risc-v toolchain from https://sifive.com/boards

Alternatively, you could also choose to build from source. Please see <https://github.com/riscv/riscv-gnu-toolchain> on how to do that.

```bash
wget -c https://static.dev.sifive.com/dev-tools/riscv64-unknown-elf-gcc-8.3.0-2019.08.0-x86_64-linux-ubuntu14.tar.gz
mkdir -p /opt/riscv
tar -xvf  riscv64-unknown-elf-gcc-8.3.0-2019.08.0-x86_64-linux-ubuntu14.tar.gz -C /opt/riscv
```

Add the below contents to the end of your ~/.bashrc file

```bash
export PATH=/opt/riscv/riscv64-unknown-elf-gcc-8.3.0-2019.08.0-x86_64-linux-ubuntu14/bin/:$PATH 
```

### Setting up QEMU

It is better to use the latest QEMU. I am not sure at this moment whether my host(Ubuntu 18.04) ships a version of QEMU that has support for RISCV. SiFive also has a QEMU binary release but I have not tested that with OpenSBI.

Better to download and build the latest QEMU version.

```shell
sudo apt-get install git libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev pkg-config bison flex
mkdir -p /opt/qemu
cd /opt/qemu
wget -c https://download.qemu.org/qemu-5.0.0.tar.xz
tar -xvf qemu-5.0.0.tar.xz -C .
cd qemu-5.0.0/
./configure --target-list=riscv64-softmmu,riscv32-softmmu,riscv64-linux-user,riscv32-linux-user  --prefix=/opt/qemu
make -j$(nproc) 
make install
```

Note: We are only building riscv targets. If you want for all architectures, then please remove  ***--target-list=riscv64-softmmu,riscv32-softmmu,riscv64-linux-user,riscv32-linux-user***

Add the below contents to the end of your ~/.bashrc file

```bash
export PATH=/opt/qemu/bin/:$PATH 
```

### Building OpenSBI

```shell
mkdir -p ~/workspace
cd ~/workspace
git clone https://github.com/riscv/opensbi.git
cd opensbi
make PLATFORM=generic CROSS_COMPILE=riscv64-unknown-elf- -j$(nproc)
```

### Firmware Binaries

If everything goes well the firmware binaries would be present in ***build/platform/generic/firmware***

Below are the list of firmwares generated

| S.no | Firmware       | Description                                                  |
| :--: | -------------- | ------------------------------------------------------------ |
|  1   | fw_jump.bin    | This firmware only jumps to a predefined address(***FW_JUMP_ADDR***). This firmware is used when the bootloader that executes before opensbi can load both the opensbi fw_jump.bin and the next stage kernel/bootloader to the RAM. In that case the FW_JUMP_ADDR is the address at which the initial loader loads the kernel/bootloader. This address information has to be specified when building the opensbi firmware. |
|  2   | fw_payload.bin | This firmware bundles the next stage kernel/bootloader(payload) to the binary itself. This file is quite big compared to other binaries since it includes the binary itself similar to [BBL](https://github.com/riscv/riscv-pk/tree/master/bbl). You can specify the path to the payload using ***FW_PAYLOAD_PATH***. When not specified, opensbi by default includes a test payload which just prints ***Test payload running*** |
|  3   | fw_dynamic.bin | This is similar to ***fw_jump.bin*** except in fw_jump you need to specify the address to jump using ***FW_JUMP_ADDR*** at opensbi compile-time. In this firmware you **don't** need to specify that. This firmware can accept the jump information via risc-v ***a2*** (x12) register from the previous booting stage. The previous booting stage should put the information about the next stage in [struct fw_dynamic_info](https://github.com/riscv/opensbi/blob/a5f9104330353491fc6c0fdb42262543d39518d9/include/sbi/fw_dynamic.h#L48) format and pass that structure's address to opensbi using the a2 register. |

### Running OpenSBI in QEMU with test payload

Now that we have compiled the necessary firmware binaries you could run these in QEMU using the below command.

```shell
qemu-system-riscv64 -M virt -m 256M -nographic \
    -bios build/platform/generic/firmware/fw_payload.bin
```
![](/assets/img/opensbi/opensbi_qemu_boot.png) 
