---
layout: post
title:  "OpenSBI[Part-1]: Introduction to OpenSBI"
date:   2020-06-29 00:00:00
categories: [riscv, opensbi]
tags: [opensbi, riscv]
published: true
---
Supervisory Binary Interface(SBI) is the interface between the Supervisor Execution Environment(SEE) and a S-mode firmware. Operating System typically runs in S-Mode. OpenSBI is one of the implementations of SBI.

## Why SBI?

[riscv-privileged-spec]: https://content.riscv.org/wp-content/uploads/2017/05/riscv-privileged-v1.10.pdf

[RISC-V privileged specification][riscv-privileged-spec] specifies three privilege levels as follows

| Level | Name                | Abbrev |
| ----- | :------------------ | ------ |
| 0     | User/Application    | **U**  |
| 1     | Supervisor          | **S**  |
| 2     | Reserved for future | -      |
| 3     | Machine             | **M**  |

Privileged modes are used to control which part of the software stack can have access to certain instructions & registers. Basically, it provides a mechanism to protect one level of the software stack from another. M-mode(Firmware) is the highest privilege level and has access to all registers & instructions while S-mode (Operating System) has access to a subset of those registers/instructions and U-mode(User Applications) even less than that.

**SBI** is the interface between the Supervisor Mode(S-mode) and Machine Mode(M-mode). There are certain actions which the Operating System running in S-mode could not do. It traps to M-mode to do that.

## Why OpenSBI?

OpenSBI is one of the implementations of SBI. There are other implementations like BBL(Berkley BootLoader) and Coreboot which have their own SBI implementations.  OpenSBI is another implementation which aims to avoid SBI implementation fragmentation. It provides SBI implementation as a library which can then be linked with a M-mode bootloader like coreboot. It also provide a reference firmware implementation which can be used to transfer control to a bootloader running in S-mode.

The next series in the post is out please see ["Part-2: Building and running OpenSBI in QEMU"]({% post_url /opensbi/2020-07-07-OpenSBI-Part-2 %})

## Reference

<https://content.riscv.org/wp-content/uploads/2019/06/13.30-RISCV_OpenSBI_Deep_Dive_v5.pdf>  
<https://content.riscv.org/wp-content/uploads/2017/05/riscv-privileged-v1.10.pdf>  
<https://content.riscv.org/wp-content/uploads/2018/05/riscv-privileged-BCN.v7-2.pdf>  
