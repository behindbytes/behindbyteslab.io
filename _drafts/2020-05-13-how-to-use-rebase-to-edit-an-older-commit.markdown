---
layout: post
title:  "Managing multiple email accounts with git-send-email"
subtitle: "Series: git-send-email[2/2]"
date:   2020-05-12 10:24:07
categories: development
description: "Multiple email configurations in git-send-email"
published: true
canonical_url: https://behindbytes.com/development/2020/05/12/managing-multiple-email-accounts-with-git-send-email"
---

Sometimes you would have the need to have multiple [sendemai] configurations in your .gitconfig file. One for personal
contributions routed via your personal gmail account and another one for your corporate account.

git send-email provides provision for this using the git send-email's --identity option.

## Configuring multiple identities in git-sendemail

Let us consider that I have two accounts developer@behindbytes.com and somerandomguy@gmail.com. I would mostly use
developer@behindbytes.com and in some cases somerandomguy@gmail.com for sending the patches.
You can edit your ~/.gitconfig files as below to accomodate different profiles/identities.

```
#default settings
[sendemail]
        smtpserver = smtp.gmail.com
        smtpserverport = 587
        smtpencryption = tls
        smtpuser = developer@behindbytes.com
        smtppass="12345678"

#settings for personal id
[sendemail "personal"]
        smtpserver = smtp.gmail.com
        smtpserverport = 587
        smtpencryption = tls
        smtpuser = somerandomguy@gmail.com
        smtppass="abcdefgh"
```
Note: It is important to double quote the identity. It should be [sendemail "personal"] and NOT [sendemail personal]

### Choosing the identity while sending email
To send using your default ID (developer@behindbytes.com)
```
git send-email -2 --to=developer@behindbytes.com
```

To send using your personal ID (somerandomguy@gmail.com)
```
git send-email -2 --to=developer@behindbytes.com --identity=personal
```
