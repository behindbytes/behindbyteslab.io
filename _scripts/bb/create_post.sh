#!/bin/bash
#
# Create a new page from template
#
# Copyright 2020 Vijai Kumar K <vijai@behindbytes.com>
set -x
BLOG_ROOT=$(git rev-parse --show-toplevel)
DRAFT_DIR=${BLOG_ROOT}/_drafts
POST_DIR=${BLOG_ROOT}/_posts

template="${1}"
title="${2}"
date_now=$(date --rfc-3339=seconds)
date_part=$(date --date=$(date_now) --rfc-3339=date)
post_name="${date_part}-${title}.markdown"
# Replace spaces with "-"
post_name="$(echo ${post_name} | sed -e 's#\s\+#-#g' )"

cp ${template} ${BLOG_ROOT}/_drafts/${post_name}

sed -i "s#%TITLE%#\"${title}\"#g" ${BLOG_ROOT}/_drafts/${post_name}
sed -i "s#%DATE%#${date_now}#g" ${BLOG_ROOT}/_drafts/${post_name}
